﻿using System;

namespace _02_instructions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Condition
            // Condition if
            int val = Convert.ToInt32(Console.ReadLine());
            if (val > 100)
            {
                Console.WriteLine("La valeur saisie est supérieur à 100");
            }
            else if (val == 100)
            {
                Console.WriteLine("La valeur saisie est égale à 100");
            }
            else
            {
                Console.WriteLine("La valeur saisie est inférieure  à 100");
            }

            // Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclu) et 7(inclu)
            int v = Convert.ToInt32(Console.ReadLine());
            if (v > -4 && v <= 7)
            {
                Console.WriteLine(" v fit parti de l'inteval -4,7");
            }

            // Trie de 3 Valeurs
            // Saisir 3 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5 < 50.7
            double d1 = Convert.ToDouble(Console.ReadLine());
            double d2 = Convert.ToDouble(Console.ReadLine());
            double d3 = Convert.ToDouble(Console.ReadLine());

            if (d2 < d1)
            {
                double tmp = d1;
                d1 = d2;
                d2 = tmp;
            }
            if (d3 < d1)
            {
                Console.WriteLine($"{d3} < {d1}< {d2}");
            }
            else if (d3 > d2)
            {
                Console.WriteLine($"{d1} < {d2}< {d3}");
            }
            else
            {
                Console.WriteLine($"{d1} < {d3}< {d2}");
            }

            // Condition switch
            const int J6 = 6;
            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case J6:
                case J6 + 1:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // - un nombre à virgule flottante v1
            // - une chaîne de caractère opérateur qui a pour valeur valide:  + - * /
            // - un nombre à virgule flottante v2
            // Afficher:
            // - Le résultat de l’opération
            // - Un message d’erreur si l’opérateur est incorrect
            // - Un message d’erreur si l’on fait une division par 0
            Console.WriteLine("Calculatrice");
            double v1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double v2 = Convert.ToDouble(Console.ReadLine());

            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($"{v1} * {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0.0) // (v2>-0.00000001 && v2<0.00000001)
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($"{v1} : {v2} = {v1 / v2}");
                    }
                    break;
                default:
                    Console.WriteLine($"{op} n'est pas valide");
                    break;
            }

            // Clause when (C# 7.0) => Ajouter une condition supplémentaire sur un case
            Console.Write("Saisir un jour (1 à 7): ");
            jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case int jo when jo >= 6 && jo <= 7:   // <== clause when
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Opérateur ternaire
            Console.Write("Saisir une valeur double: ");
            double va = Convert.ToDouble(Console.ReadLine());
            double abs = va < 0.0 ? -va : va;
            Console.WriteLine($"|{val}| = {abs}");
            #endregion

            #region boucle
            // Boucle while 
            int j = 0;
            while (j < 10)
            {
                Console.WriteLine(j++);
            }

            // Boucle do while
            j = 0;
            do
            {
                Console.WriteLine(j++);
            }
            while (j < 10);

            // Boucle for
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            //for(; ; ) // Boucle infinie
            //{

            //}
            // ou
            //while (true)
            //{

            //}
            #endregion

            #region Instruction de saut
            // break
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    break;
                }
                Console.WriteLine($"i={i}");
            }

            // continue
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;
                }
                Console.WriteLine($"i={i}");
            }

            // goto pour quitter des boucles imbriquées
            for (int k = 0; k < 10; k++)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (i == 3)
                    {
                        goto EXIT_LOOP;
                    }
                    Console.WriteLine($"{i} {k}");
                }
            }
        EXIT_LOOP:
            #endregion
            //Table de multiplication
            //Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9

            //    1 X 4 = 4
            //    2 X 4 = 8
            //    …
            //    9 x 4 = 36


            //Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur
            for (; ; )
            {
                double m = Convert.ToDouble(Console.ReadLine());
                if (m < 1 || m > 9)
                {
                    break;
                }
                for (int i = 1; i < 10; i++)
                {
                    Console.WriteLine($"{m} x {i} = {m * i}");
                }
            }

            // Quadrillage 
            // Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
            //    ex: pour 2 3
            //    [ ][ ]
            //    [ ][ ]
            //    [ ][ ]

            int c = Convert.ToInt32(Console.ReadLine());
            int l = Convert.ToInt32(Console.ReadLine());
            for(int row=0;row<l;row++)
            {
                for(int col=0; col<c; col++)
                {
                    Console.Write("[ ] ");
                }
                Console.WriteLine();
            }

            // goto avec un switch
            jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    // goto case J6+1;
                    goto default;
                case J6:
                case J6 + 1:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }
            Console.ReadKey();
        }
    }
}
