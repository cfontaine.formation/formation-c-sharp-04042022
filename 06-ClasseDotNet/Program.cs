﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_ClasseDotNet
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string str = "Hello"; //littéral;
            Console.WriteLine(str);
            string str2 = new string('a', 10);
            Console.WriteLine(str2);
            Console.WriteLine("Hello".ToLower());

            // Length -> Nombre de caractère de la chaine de caractère
            Console.WriteLine(str2.Length); // 10

            // On peut accèder à un caractère de la chaine comme à un élément d'un tableau (index commence à 0)
            Console.WriteLine(str[1]); // e

            // Concaténation 
            // avec l'opérateur + ou avec la méthode de classe Concat
            Console.WriteLine(str + " World");
            string str3 = string.Concat(str, " World");
            Console.WriteLine(str3);

            // La méthode Join concatène les chaines, en les séparants par une chaine de séparation
            string str4 = string.Join(";", "azerty", "yuiop", "dfgh", "jkjl");
            Console.WriteLine(str4);

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string[] elm = str4.Split(';');
            foreach(var e in elm)
            {
                Console.WriteLine(e);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine
            Console.WriteLine(str3.Substring(6));

            // ou pour le nombre de caractères passé en paramètre
            Console.WriteLine(str3.Substring(5,3));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str3.Insert(5,"-----"));
            Console.WriteLine(str3.Remove(5, 4));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str3.StartsWith("Hell"));
            Console.WriteLine(str3.StartsWith("a"));

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre        
            Console.WriteLine(str3.IndexOf("o")); // 4
            Console.WriteLine(str3.IndexOf("o",5)); // 7 idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str3.IndexOf("o", 8)); // -1 retourne -1, si le caractère n'est pas trouvé

            // Remplace toutes les chaines (ou caratères) oldValue par newValue 
            Console.WriteLine(str3.Replace('o','a'));

            // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str3.Contains("Wo")); // true
            Console.WriteLine(str3.Contains("aa")); //false

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str3.PadLeft(40, '_'));

            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str3.PadRight(40, '_'));
            Console.WriteLine(str3.PadRight(5, '_')); // retourne la chaine str3

            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            Console.WriteLine("   \n \t    Hello \n \nBonjour     \n \t \b".Trim());
            Console.WriteLine("   \n \t    Hello \n \nBonjour     \n \t \b".TrimStart());   // idem uniquement en début de chaine
            Console.WriteLine("   \n \t    Hello \n \nBonjour     \n \t \b".TrimEnd());     // idem uniquement en fin de c

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str3.ToUpper());

            // ToLower convertie tous les caractères en minuscule
            Console.WriteLine(str3.ToLower());

            // On peut tester l'égalité de 2 chaines de caractères avec Equals et l'opérateur ==
            Console.WriteLine(str3.Equals("azerty"));
            Console.WriteLine(str3 == "Hello World");

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(str3.CompareTo("Bonjour"));
            Console.WriteLine(string.Compare("Bonjour", str3));

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine(str3.Trim().ToLower().Substring(6).ToUpper());

            // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
            // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
            StringBuilder sb = new StringBuilder("Test");
            sb.Append("___");
            sb.Append(123);
            sb.Remove(4, 2);
            string str6 = sb.ToString();
            Console.WriteLine(str6);

            // Exercice Chaine de caractère
            Console.WriteLine(Inverser("Bonjour"));

            Console.WriteLine(Palindrome("Radar"));
            Console.WriteLine(Palindrome("Bonjour"));

            Console.WriteLine(Acronyme("Comité international olympique"));
            Console.WriteLine(Acronyme("Organisation du traité de l'Atlantique Nord"));

            #region Date
            DateTime d=DateTime.Now;    // DateTime.Now => récupérer l'heure et la date courante
            Console.WriteLine(d);
            Console.WriteLine(d.Year);
            Console.WriteLine(d.Day);
            Console.WriteLine(d.Hour);

            DateTime dUtc = DateTime.UtcNow;
            Console.WriteLine(dUtc);

            DateTime vacance = new DateTime(2022, 7, 1);
            Console.WriteLine(vacance);

            TimeSpan duree = vacance - d;
            Console.WriteLine(duree);

            TimeSpan dixJours = new TimeSpan(10, 0, 0, 0);
            Console.WriteLine(d.Add(dixJours));
            Console.WriteLine(d.AddMonths(3));
            Console.WriteLine(d.CompareTo(vacance)); //-1
            Console.WriteLine(vacance.CompareTo(d)); // 1

            Console.WriteLine(d.ToLongTimeString());
            Console.WriteLine(d.ToShortTimeString());
            Console.WriteLine(d.ToLongDateString());
            Console.WriteLine(d.ToShortDateString());

            Console.WriteLine(d.ToString("dd MMM yy"));
            DateTime d2 = DateTime.Parse("2022/04/07");
            Console.WriteLine(d2);
            #endregion

            #region Collection
            // Collection faiblement typé => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("hello");
            lst.Add("World");
            lst.Add(3.5);
            if(lst[0] is string)
            {
                string sr = (string)lst[0];
                Console.WriteLine(sr);
                Console.WriteLine(sr.GetType());


            }
            // Collection fortement typée => type générique
            List<string> lstStr= new List<string>();
            lstStr.Add("hello");
            lstStr.Add("Bonjour");
            lstStr.Add("World");
            // lstStr.Add(1.2);// On ne peut plus ajouter que des chaines de caractères => sinon erreur de complilation
            string resStr = lstStr[1];
            Console.WriteLine(lstStr[0]);
            Console.WriteLine(lstStr.Count);
            Console.WriteLine(lstStr.Max());

            lstStr[0] = "azerty";   // Accès à un élément de la liste

            foreach (var s in lstStr)   // Parcourir la collection complétement
            {
                Console.WriteLine(s);
            }

            Console.WriteLine(lstStr.Count); // Nombre d'élément de la collection
            Console.WriteLine(lstStr.Max());    // Valeur maximum stocké dans la liste
            lstStr.Reverse();                   // Inverser l'ordre de la liste

            foreach (var s in lstStr)
            {
                Console.WriteLine(s);
            }

            // Parcourir un collection avec un Enumérateur
            IEnumerator<string> it = lstStr.GetEnumerator();
            it.Reset();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            // IEnumerable
            foreach (var n in Numbers())
            {
                Console.WriteLine(n);
            }

            // IEnumerable -> Parcours d'une chaine
            foreach (var c in str3)
            {
                Console.WriteLine(c);
            }

            // Dictionary => association clé/valeur
            Dictionary<int, string> di = new Dictionary<int, string>();
            di.Add(3, "azerty");    // Add => ajout d'un valeur associé à une clé
            di.Add(123, "hello");
            di.Add(12, "world");
            di.Add(4, "asuprimer");
            // accès à un élément m[clé] => valeur
            Console.WriteLine(di[123]);
            di[3]= "qwerty"; // modifier
            // di.Add(3, "qwerty"); // On ne peut pas ajouter, si la clé éxiste déjà => exceptio
            Console.WriteLine(di.Count);
            di.Remove(4);

            // Parcourir un dictionnary
            foreach (var kv in di)
            {
                Console.WriteLine($"Clé={kv.Key} Valeur={kv.Value}");
            }
            #endregion

            // Méthode générique
            MethodeGeneric<string>("azerty");
            MethodeGeneric("azerty");
            MethodeGeneric(1.2);

            Console.ReadKey();
        }

        #region Inversion de chaine
        // Écrire la méthode Inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // exemple : bonjour => ruojnob
        static string Inverser(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }
        #endregion

        #region  Palindrome
        // Écrire une méthode qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
        // Ex: SOS, radar

        static bool Palindrome(string str)
        {
            string tmp = str.Trim().ToLower();
            return tmp == Inverser(tmp);
        }
        #endregion 

        #region Acronyme
        //faire une méthode de classe qui prend en paramètre une phrase et qui retourne un acronyme
        //Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres

        //Ex: Comité international olympique → CIO
        //Organisation du traité de l'Atlantique Nord → OTAN

        static string Acronyme(string str)
        {
            StringBuilder sb = new StringBuilder();
            string[] elms = str.Split(' ', '\'');
            foreach(var e in elms)
            {
                if (e.Length > 2)
                {
                    sb.Append(e.ToUpper()[0]);
                }
            }
            return sb.ToString();
        }
        #endregion 

        // iterateur: Permet un parcours personnalisé d'une collection
        static IEnumerable<int> Numbers()
        {
            int i = 0;
            do
            {
                yield return i;
            } while (i++ < 30);
        }

        // Méthode générique
        static void  MethodeGeneric<T>( T a)
        {
            Console.WriteLine(a);
        }
    }
}
