﻿using System;

namespace _03_tableaux
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region tableau à une dimension
            // Déclarer un tableau

            //double[] tab;
            //tab = new double[5];

            double[] tab = new double[5];

            // Valeur d'nitialisation des éléments du tableau
            // - entier -> 0
            // - double , float ou un decimal -> 0.0
            // - char -> '\u0000'
            // - boolean -> false
            //  -type référence  -> null

            // Accèder à un élément du tableau
            Console.WriteLine(tab[1]);
            tab[2] = 3.14;

            // si l'on essaye d'accèder à un élément en dehors du tableau -> IndexOutOfRangeException
            // Console.WriteLine(tab[9]); 
            // tab[15] = 12;

            // Nombre d'élément du tableau
            Console.WriteLine(tab.Length);

            // Parcourir complétement un tableau (avec un for) 
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine($"tab[{i}]={tab[i]}");
            }

            // Parcourir complétement un tableau (foreach)
            // foreach (double elm in tab) // ou
            foreach (var elm in tab) //  var -> elm est de type double
            {
                Console.WriteLine(elm);  // elm uniquement en lecture
                //elm = 5.7;    // => Erreur ,on ne peut pas modifier elm
            }

            // On peut utiliser une variable entière pour donner la taille du tableau
            int sizeTab = 4;
            int[] tab2 = new int[sizeTab];

            // Déclaration et initialisation
            string[] tabStr = { "Hello", "Bonjour", "world" };
            foreach (var elm in tabStr)
            {
                Console.WriteLine(elm);
            }
            #endregion

            #region Exercice Tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7, -6, -4, -8, -3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau

            //  int[] t ={ -7,-6,-4,-8,-3}; //1

            int size = Convert.ToInt32(Console.ReadLine()); //2
            int[] t = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = Convert.ToInt32(Console.ReadLine());
            }

            int max = int.MinValue;  //t[0];
            double somme = 0.0;
            foreach (var val in t)
            {
                if (val > max)
                {
                    max = val;
                }
                somme += val;
            }

            Console.WriteLine($"Maximum={max} Moyenne={somme / t.Length}");
            #endregion

            #region Tableau Multidimmension
            int[,] tab2d = new int[4, 3];    // Déclaration d'un tableau à 2 dimensions

            tab2d[0, 2] = 123;              // Accès à un élémént d'un tableau à 2 dimensions
            Console.WriteLine(tab2d[0, 0]);

            // Nombre d'élément du tableau
            Console.WriteLine(tab2d.Length); // 12

            // Nombre de dimension du tableau 
            Console.WriteLine(tab2d.Rank); // 2

            // Nombre de ligne du tableau
            Console.WriteLine(tab2d.GetLength(0)); //  4

            // Nombre de colonne du tableau
            Console.WriteLine(tab2d.GetLength(1)); // 3

            // Nombre d'élément pour chaque dimension
            for (int i = 0; i < tab2d.Rank; i++)
            {
                Console.WriteLine($"Dimension: {tab2d.GetLength(i)}");
            }

            // Parcourir complétement un tableau à 2 dimension => for
            for (int i = 0; i < tab2d.GetLength(0); i++)
            {
                for (int j = 0; j < tab2d.GetLength(1); j++)
                {
                    Console.Write($"{tab2d[i, j]} \t");
                }
                Console.WriteLine();
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var elm in tab2d)  // avec var la variable elm aura le même type que le tableau 
            {
                Console.WriteLine(elm);
            }

            // Déclarer un tableau à 2 dimension en l'initialisant
            char[,] tabChr = { { 'a', 'z' }, { 'e', 'r' }, { 't', 'y' } };
            foreach (var c in tabChr)
            {
                Console.WriteLine(c);
            }

            // Tableau 3D
            // Déclaration d'un tableau à 3 dimensions
            double[,,] tab3D = new double[2, 3, 4];

            // Accès à un élément d'un tableau à 3 dimensions
            tab3D[1, 0, 2] = 4.67;

            // Nombre d'élément => 24 Nombre de dimension du tableau => 3
            Console.WriteLine($"{tab3D.Length} {tab3D.Rank}");

            // Afficher le nombre d'élément de chaque dimmension
            for (int i = 0; i < tab3D.Rank; i++)
            {
                Console.WriteLine($"Dimmension {i} ={tab3D.GetLength(i)}");
            }
            #endregion


            #region tableau de tableau
            // Déclaration d'un tableau de tableau
            int[][] tabEsc = new int[3][];

            tabEsc[0] = new int[3];
            tabEsc[1] = new int[5];
            tabEsc[2] = new int[2];

            // Accès à un élément
            tabEsc[0][2] = 3;
            Console.WriteLine(tabEsc[0][2]);

            // Nombre de ligne
            Console.WriteLine(tabEsc.Length);

            // Nombre de colonne de la première ligne
            Console.WriteLine(tabEsc[0].Length);

            // Nombre de colonne de la deuxième ligne
            Console.WriteLine(tabEsc[1].Length);

            //  Parcourir complétement un tableau de tableau => for
            for (int i = 0; i < tabEsc.Length; i++)
            {
                for (int j = 0; j < tabEsc[i].Length; j++)
                {
                    Console.Write($"{tabEsc[i][j]} \t");
                }
                Console.WriteLine();
            }

            //  Parcourir complétement un tableau de tableau => foreach
            foreach (int[] row in tabEsc)
            {
                foreach (int elm in row) // elm type -> int
                {
                    Console.Write($"{ elm} \t");
                }
                Console.WriteLine();
            }

            // Déclarer et initialiser un tableau de tableau
            int[][] tabEsc2 = new int[][] { new int[] { 1, 3, 5 }, new int[] { 2, 4 }, new int[] { 5, 6, 7 } };
            foreach (int[] row in tabEsc2)
            {
                foreach (int elm in row)
                {
                    Console.Write($"{ elm} \t");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
        #endregion
    }
}
