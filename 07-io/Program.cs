﻿using _07_BibliothequeFichier;
using System;
using System.Collections.Generic;

namespace _07_io
{
    // Pour ajouter au projet la bibliothèque
    // Cliquer droit sur le projet -> Ajouter -> référence -> choisir 07-BibliothequeFichier

    internal class Program
    {
        static void Main(string[] args)
        {
            Utilitaire.InfoLecteur();
            Utilitaire.InfoDossier();
            Utilitaire.InfoFichier();
            Utilitaire.Parcourir(@"C:\Dawan\Testio\");
            Utilitaire.EcrireFichierText(@"C:\Dawan\Testio\test.txt");
            List<string> list = Utilitaire.LireFichierText(@"C:\Dawan\Testio\test.txt");
            foreach (string l in list)
            {
                Console.WriteLine(l);
            }

            Utilitaire.EcrireFichierBin(@"C:\Dawan\Testio\test.bin");
            Utilitaire.LireFichierBin(@"C:\Dawan\Testio\test.bin");
            Console.ReadKey();
        }
    }
}
