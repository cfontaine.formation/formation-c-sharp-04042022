﻿using System;
using System.Collections.Generic;
using System.IO;

namespace _07_BibliothequeFichier
{
    public class Utilitaire
    {

        public static void InfoLecteur()
        {
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] drv = DriveInfo.GetDrives();
            foreach (var d in drv)
            {
                Console.WriteLine(d.Name);              // Nom du lecteur
                Console.WriteLine(d.TotalFreeSpace);    // Espace disponible sur le lecteur
                Console.WriteLine(d.DriveType);         // Espace total du lecteur
                Console.WriteLine(d.DriveFormat);       // Système de fichiers du lecteur NTFS, FAT ...
                Console.WriteLine(d.TotalSize);         // Type de lecteur Fixed Removable
            }
        }
        public static void InfoDossier()
        {
            //  Teste si le dossier existe
            if (!Directory.Exists(@"c:\Dawan\Testio\repA"))
            {
                Directory.CreateDirectory(@"C:\Dawan\Testio\repA");         // Création du répertoire
            }
            string[] paths = Directory.GetDirectories(@"C:\Dawan\Testio\"); // Liste les répertoires contenu dans le chemin
            foreach (var p in paths)
            {
                Console.WriteLine(p);
            }
            string[] files = Directory.GetFiles(@"C:\Dawan\Testio\");       // Liste les fichiers du répertoire
            foreach (var p in files)
            {
                Console.WriteLine(p);
            }
        }

        public static void InfoFichier()
        {
            if (File.Exists(@"C:\Dawan\Testio\asup.txt"))   // Teste si le fichier
            {
                File.Delete(@"C:\Dawan\Testio\asup.txt");   // Supprime le fichier
            }
        }

        public static void Parcourir(string chemin)
        {
            if (Directory.Exists(chemin))
            {
                string[] fileNames = Directory.GetFiles(chemin);
                foreach (string file in fileNames)
                {
                    Console.WriteLine(file);
                }
                string[] directoryNames = Directory.GetDirectories(chemin);
                foreach (var directory in directoryNames)
                {
                    Console.WriteLine($"Répertoire {directory}");
                    Console.WriteLine("__________");
                    Parcourir(directory);
                }
            }


        }
        public static void EcrireFichierText(string chemin)
        {
            StreamWriter sw = null; // StreamWriter Ecrire un fichier texte
            try                     // Sans utiliser Using
            {
                sw = new StreamWriter(chemin, true);    // append à true => compléte le fichier s'il existe déjà, à false le fichier est écrasé
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Hello World");
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
        }

        // Using => Équivalent d'un try / finally + Close()
        public static List<string> LireFichierText(string chemin)
        {
            List<string> list = new List<string>();
            using (StreamReader sr = new StreamReader(chemin))  // StreamReader Lire un fichier texte
            {
                while (!sr.EndOfStream) // Propriété EndOfStream est vrai si le fichier atteint la fin du fichier
                {
                    string line = sr.ReadLine();
                    list.Add(line);
                }
            }
            return list;
        }

        public static void EcrireFichierBin(string chemin)
        {
            using (FileStream fs = new FileStream(chemin, FileMode.Append)) // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte b = 0; b < 100; b++)
                {
                    fs.WriteByte(b);    // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBin(string chemin)
        {
            byte[] t = new byte[10];
            using (FileStream fs = new FileStream(chemin, FileMode.Open))
            {
                int nb = 1;
                while (nb != 0)
                {
                    nb = fs.Read(t, 0, t.Length);   // Lecture de 10 octets au maximum dans le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                    for (int i = 0; i < nb; i++)    // Read => retourne le nombre d'octets lue dans le fichier
                    {
                        Console.Write($"{t[i]} ");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
