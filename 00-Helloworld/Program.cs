﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00_Helloworld
{
    /// <summary>
    /// Classe HelloWorld
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Point d'entée du programme
        /// </summary>
        /// <param name="args">Arguments ligne de commande</param>
        static void Main(string[] args) // commentaire fin de ligne
        {
            /* commentaire 
             * sur 
             * plusieurs lignes */
            Console.WriteLine("Hello world");
            Console.ReadKey();
        }
    }
}
