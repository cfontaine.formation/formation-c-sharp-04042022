﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _09_winform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitListView();
        }

        private void bptest_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this, "J'ai cliqué sur bouton", "Message de test", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bpAjouter_Click(object sender, EventArgs e)
        {

        }

        private void BpModifier_Click(object sender, EventArgs e)
        {

        }

        private void bpSupprimer_Click(object sender, EventArgs e)
        {

        }

        private void bpQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void InitListView()
        {
            listViewContact.Items.Clear();
            for(int i = 0; i < 4; i++)
            {
                AddListView();
            }
        }

        private void AddListView()
        {
            ListViewItem listItem = new ListViewItem("John",0);
            listItem.SubItems.Add("Doe");
            listItem.SubItems.Add("jd@dawan.fr");
            listItem.SubItems.Add("06.00.00.00.00");
            listItem.SubItems.Add("06/04/2002");
            listViewContact.Items.Add(listItem);
        }
    }
}
