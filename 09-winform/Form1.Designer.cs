﻿namespace _09_winform
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.bptest = new System.Windows.Forms.Button();
            this.bpAjouter = new System.Windows.Forms.Button();
            this.bpSupprimer = new System.Windows.Forms.Button();
            this.BpModifier = new System.Windows.Forms.Button();
            this.bpQuitter = new System.Windows.Forms.Button();
            this.listViewContact = new System.Windows.Forms.ListView();
            this.Prenom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Nom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Email = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Telephone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DateNaissance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // bptest
            // 
            this.bptest.Location = new System.Drawing.Point(29, 367);
            this.bptest.Name = "bptest";
            this.bptest.Size = new System.Drawing.Size(109, 53);
            this.bptest.TabIndex = 0;
            this.bptest.Text = "Test";
            this.bptest.UseVisualStyleBackColor = true;
            this.bptest.Click += new System.EventHandler(this.bptest_Click);
            // 
            // bpAjouter
            // 
            this.bpAjouter.Location = new System.Drawing.Point(177, 367);
            this.bpAjouter.Name = "bpAjouter";
            this.bpAjouter.Size = new System.Drawing.Size(121, 53);
            this.bpAjouter.TabIndex = 1;
            this.bpAjouter.Text = "Ajouter";
            this.bpAjouter.UseVisualStyleBackColor = true;
            this.bpAjouter.Click += new System.EventHandler(this.bpAjouter_Click);
            // 
            // bpSupprimer
            // 
            this.bpSupprimer.Location = new System.Drawing.Point(474, 367);
            this.bpSupprimer.Name = "bpSupprimer";
            this.bpSupprimer.Size = new System.Drawing.Size(121, 53);
            this.bpSupprimer.TabIndex = 2;
            this.bpSupprimer.Text = "Supprimer";
            this.bpSupprimer.UseVisualStyleBackColor = true;
            this.bpSupprimer.Click += new System.EventHandler(this.bpSupprimer_Click);
            // 
            // BpModifier
            // 
            this.BpModifier.Location = new System.Drawing.Point(320, 367);
            this.BpModifier.Name = "BpModifier";
            this.BpModifier.Size = new System.Drawing.Size(121, 53);
            this.BpModifier.TabIndex = 3;
            this.BpModifier.Text = "Modifier";
            this.BpModifier.UseVisualStyleBackColor = true;
            this.BpModifier.Click += new System.EventHandler(this.BpModifier_Click);
            // 
            // bpQuitter
            // 
            this.bpQuitter.Location = new System.Drawing.Point(630, 367);
            this.bpQuitter.Name = "bpQuitter";
            this.bpQuitter.Size = new System.Drawing.Size(121, 53);
            this.bpQuitter.TabIndex = 4;
            this.bpQuitter.Text = "Quitter";
            this.bpQuitter.UseVisualStyleBackColor = true;
            this.bpQuitter.Click += new System.EventHandler(this.bpQuitter_Click);
            // 
            // listViewContact
            // 
            this.listViewContact.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Prenom,
            this.Nom,
            this.Email,
            this.Telephone,
            this.DateNaissance});
            this.listViewContact.HideSelection = false;
            this.listViewContact.Location = new System.Drawing.Point(29, 37);
            this.listViewContact.MultiSelect = false;
            this.listViewContact.Name = "listViewContact";
            this.listViewContact.Size = new System.Drawing.Size(722, 287);
            this.listViewContact.TabIndex = 5;
            this.listViewContact.UseCompatibleStateImageBehavior = false;
            this.listViewContact.View = System.Windows.Forms.View.Details;
            this.listViewContact.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // Prenom
            // 
            this.Prenom.Text = "Prénom";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listViewContact);
            this.Controls.Add(this.bpQuitter);
            this.Controls.Add(this.BpModifier);
            this.Controls.Add(this.bpSupprimer);
            this.Controls.Add(this.bpAjouter);
            this.Controls.Add(this.bptest);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bptest;
        private System.Windows.Forms.Button bpAjouter;
        private System.Windows.Forms.Button bpSupprimer;
        private System.Windows.Forms.Button BpModifier;
        private System.Windows.Forms.Button bpQuitter;
        private System.Windows.Forms.ListView listViewContact;
        private System.Windows.Forms.ColumnHeader Prenom;
        private System.Windows.Forms.ColumnHeader Nom;
        private System.Windows.Forms.ColumnHeader Email;
        private System.Windows.Forms.ColumnHeader Telephone;
        private System.Windows.Forms.ColumnHeader DateNaissance;
    }
}

