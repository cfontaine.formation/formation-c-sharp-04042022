﻿using System;
using System.Text;

namespace _01_Base
{
    // Une enumération est un ensemble de constante
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Motorisation : sbyte { DIESEL = 10, ESSENCE = 95, GPL = 2, ETHANOL = 11, ELECTRIQUE }

    [Flags]
    enum JourSemaine
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }

    // Définition d'une structure
    struct Point
    {
        public int x;
        public int y;
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Déclaration variable
            // Déclaration d'une variable   type nomVariable;
            int i;

            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable
            double hauteur, largeur;
            largeur = 12.3;
            hauteur = 45.6;
            Console.WriteLine(hauteur + " largeur=" + largeur);// + => concaténation

            // Déclaration et initialisation de variable    type nomVariable = valeur;
            int j = 1;
            int j1 = 3, j2 = 4;
            Console.WriteLine(j + " " + j1 + " " + j2);
            #endregion

            #region Littéral
            // Littéral booléen
            bool tst = true; // false
            Console.WriteLine(tst);

            // Littéral caratère
            char chr = 'a';
            char chrUtf8 = '\u0045';        // Caractère en UTF-8
            char chrUtf8Hexa = '\x45';
            Console.WriteLine(chr + " " + chrUtf8 + " " + chrUtf8Hexa);

            // Littéral entier -> int par défaut
            long l = 12L;    // L -> Long
            uint ui = 123U;  // U -> unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral entier -> changement de base
            int dec = 123;          // décimal (base 10) par défaut
            int hexa = 0xFF2;       // 0x => héxadécimal (base 16)
            int bin = 0b010101;     // 0b => binaire (base 2)
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Littéral nombre à virgule flottante
            double d1 = 12.3;
            double d2 = .05;
            double d3 = 1.23e3;
            Console.WriteLine(d1 + " " + d2 + " " + d3);

            // Littéral nombre à virgule flottante -> par défaut double
            float f = 12.3F;
            decimal deci = 123.4M;
            Console.WriteLine(f + " " + deci);
            #endregion

            #region Typage implicite
            // Type implicite -> var
            // Le type est déterminé par le type de la littérale, de l'expression ou du retour d'une méth
            var v1 = 10;        // v1 -> double
            var v2 = "hello";   // v2 -> string
            var v3 = i + j;     // v3-> int le résultat de i + j est un entier
            Console.WriteLine(v1 + " " + v2 + " " + v3);
            #endregion

            // avec @ on peut utiliser les mots réservés comme nom de variable (uniquement si nécessaire)
            int @while = 1;
            Console.WriteLine(@while);

            #region Transtypage
            // Transtypage autormatique ( pas de perte de donnée)
            // type inférieur => type supérieur
            int ti1 = 45;
            double ti2 = ti1;
            long ti3 = ti1;
            Console.WriteLine(ti1 + " " + ti2 + " " + ti3);

            // Transtypage explicite: cast -> (nouveauType)
            double te1 = 12.3;
            float te2 = (float)te1;
            int te3 = (int)te1;
            decimal te4 = (decimal)te1;
            Console.WriteLine(te1 + " " + te2 + " " + te3 + " " + te4);
            #endregion

            #region Dépassement de capacité
            int dep1 = 300;             // 00000000 00000000 00000001 00101100    300
            sbyte dep2 = (sbyte)dep1;   //                            00101100    44
            Console.WriteLine(dep2);

            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked
            //{
            //    dep1 = 300;
            //    dep2 = (sbyte)dep1;
            //    Console.WriteLine(dep2);    // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            // Par défaut la vérification des dépassement de capacités est unchecked
            // si l'on veut qu'elles soient par défaut checked: il faut choisir l'option checked dans les propriétés du projet dans Advanced Build Settings
            // Dans ce cas, on utilise unchecked pour désactiver la vérification des dépassements
            unchecked
            {
                dep1 = 300;
                dep2 = (sbyte)dep1;
                Console.WriteLine(dep2);     // plus de vérification de dépassement de capacité
            }
            #endregion

            #region Fonction de conversion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = 123;
            double fc2 = Convert.ToDouble(fc1); // Convertion d'un entier en double
            Console.WriteLine(fc2);

            string str = "12,3";
            double fc3 = Convert.ToDouble(str);
            Console.WriteLine(fc3);

            // int fc = Convert.ToInt32("azerty"); //  Erreur => génère une exception formatException
            // Conversion d'un nombre sous forme de chaine dans une autre base
            Console.WriteLine(Convert.ToString(fc1, 2)); // binaire
            Console.WriteLine(Convert.ToString(fc1, 16)); // hexa

            // Conversion d'une chaine de caractères en entier
            // Parse
            int fc4 = int.Parse("456");
            //fc4 = int.Parse("azery");     //  Erreur => génère une exception formatException
            Console.WriteLine(fc4);

            bool tstCnv = int.TryParse("456", out fc4);  // Retourne true et la convertion est affecté à fc4
            Console.WriteLine(tstCnv + " " + fc4);

            tstCnv = int.TryParse("456eee", out fc4);  // Retourne false, 0 est affecté à fc4
            Console.WriteLine(tstCnv + " " + fc4);
            #endregion

            #region Type référence
            StringBuilder s1 = new StringBuilder("azerty");
            StringBuilder s2 = null;
            Console.WriteLine(s1 + " " + s2);
            s2 = s1;
            Console.WriteLine(s1 + " " + s2);
            s1 = null;
            Console.WriteLine(s1 + " " + s2);
            s2 = null;  // s1 et s2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il est éligible à la destruction par le garbage collector
            Console.WriteLine(s1 + " " + s2);
            #endregion

            #region Type nullable
            // Nullable : permet d'utiliser une valeur null avec un type valeur
            double? tn1 = null;

            Console.WriteLine(tn1.HasValue); // tn1 == null retourne false
            tn1 = 3.67;                      // Conversion implicite(double vers nullable)
            Console.WriteLine(tn1.HasValue); // La propriété HasValue retourne true si  tn contient une valeur (!= null)

            Console.WriteLine(tn1.Value);   // Pour récupérer la valeur, on peut utiliser la propriété Value
            Console.WriteLine((double)tn1); // ou, on peut faire un cast
            #endregion

            #region Constante
            const double PI = 3.14;
            //PI=3.1419;    // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(PI * 2);
            #endregion

            #region Format de chaine de caractère
            int xi = 1;
            int yi = 4;
            string resFormat = string.Format("xi={0} yi={1}", xi, yi);  // On peut définir directement le format dans la mèthode WriteLine
            Console.WriteLine(resFormat);
            Console.WriteLine("xi={0} yi={1}", xi, yi);

            Console.WriteLine($"xi={xi} yi={yi}");

            Console.WriteLine("c:\tmp\newfile.txt");    // \t et \n sont considérés comme des caractères spéciaux

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers)
            Console.WriteLine(@"c:\tmp\newfile.txt");
            #endregion

            #region Saisie clavier
            Console.Write("Saisie Clavier :");
            string line = Console.ReadLine();
            Console.WriteLine(line);
            #endregion

            #region Exercice Salutation
            // Faire un programme qui:
            //- Affiche le message: Entrer votre nom
            //- Permet de saisir le nom
            //- Affiche Bonjour, complété du nom saisie

            Console.WriteLine("Enter votre nom ");
            string nom = Console.ReadLine();
            Console.WriteLine($"Bonjour, {nom}");
            #endregion

            #region Exercice Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4
            Console.Write("Saisir deux entiers: ");

            int add1 = Convert.ToInt32(Console.ReadLine());
            int add2 = Convert.ToInt32(Console.ReadLine());
            int somme = add1 + add2;
            Console.WriteLine($"{add1} + {add2} = {somme}");
            #endregion

            #region Opérateur
            // Opérateur arthmétique
            int op1 = 12;
            int op2 = 45;
            int opRes = op1 + op2;
            Console.WriteLine($"{op1} + {op2} = {opRes}");

            // Division par 0

            // Entier 
            // int zero = 0;
            // Console.WriteLine(0/zero);   // exception

            // Nombre à virgule flottante
            Console.WriteLine(1.0 / 0.0);   // double.PositiveInfinity
            Console.WriteLine(-1.0 / 0.0);  // double.NegativeInfinity
            Console.WriteLine(0.0 / 0.0);   // double.NaN

            // Incrementation
            // pré-incrémentation
            int inc = 0;
            int res = ++inc; // inc=1 res=1
            Console.WriteLine($"{inc} {res}");

            // post-incrémentation
            inc = 0;
            res = inc++; // res=0 inc=1
            Console.WriteLine($"{inc} {res}");

            // Affectation composée
            inc += 23; //inc=inc +23
            Console.WriteLine(inc);

            // Opérateur de comparaison
            bool tstComp = inc > 100;   // Une comparaison a pour résultat un booléen
            Console.WriteLine(tstComp); // true

            // Opérateur logique
            // Opérateur Non !
            Console.WriteLine(!tstComp); //true 

            // Opérateur court-circuit && et ||
            bool tst3 = inc < 100 && inc > 0; // true
            Console.WriteLine(tst3);

            // && -> dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool tst4 = inc == 100 && inc > 0; // false
            Console.WriteLine(tst4);

            // || -> dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            bool tst5 = inc < 100 || inc > 0; // true
            Console.WriteLine(tst5);

            // Opérateur Binaires (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(Convert.ToString(~b, 2));             // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b1000, 2));     //          et => 1000
            Console.WriteLine(Convert.ToString(b | 0b100, 2));      //          ou => 11110
            Console.WriteLine(Convert.ToString(b ^ 0b1010, 2));     // ou exclusif => 10000
            Console.WriteLine(Convert.ToString(b >> 2, 2));         // Décalage à droite de 2 (insertion de 2 0 à gauche) => 110
            Console.WriteLine(Convert.ToString(b << 1, 2));         // Décalage à gauche de 1 (insertion de 1 0 à droite) => 110100

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str5 = "azerty";
            string strRes = str5 ?? "valeur défaut";
            Console.WriteLine(strRes);  // azerty
            str5 = null;
            strRes = str5 ?? "valeur défaut";
            Console.WriteLine(strRes);  // valeur défaut
            #endregion

            #region Promotion numérique
            // prn1 est promu en double => Le type le + petit est promu vers le +grand type des deux
            int prn1 = 11;
            double prn2 = 123.45;
            double prnRes = prn1 + prn2;
            Console.WriteLine(prnRes);

            Console.WriteLine(prn1 / 2);    // 5
            Console.WriteLine(prn1 / 2.0);  // 5.5   prn1 est promu en double

            // sbyte, byte, short, ushort, char sont promus en int
            short sh1 = 1;
            short sh2 = 3;          // sbyte, byte, short, ushort, char sont promus en int
            int sh3 = sh1 + sh2;    // sh1 et sh2 sont promus en int
            Console.WriteLine(sh3);
            #endregion

            #region Exercice Moyenne: 
            // Saisir 2 nombres entiers et afficher la moyenne dans la console
            Console.Write("Saisir deux entiers: ");
            int val1 = Convert.ToInt32(Console.ReadLine());
            int val2 = Convert.ToInt32(Console.ReadLine());
            double moyenne = (val1 + val2) / 2.0;
            Console.WriteLine($"moyenne={moyenne}");
            #endregion
            #region enumeration
            // m est une variable qui ne pourra accepter que les valeurs de l'enum Motorisation
            Motorisation m = Motorisation.GPL;

            // enum -> string
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            string mStr = m.ToString();
            Console.WriteLine(mStr);

            // enum ->  entier (cast)
            int im = (int)m;
            Console.WriteLine(im);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            Motorisation mr = (Motorisation)Enum.Parse(typeof(Motorisation), "DIESEL");
            Console.WriteLine(mr);

            // mr = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL2");    // si la chaine n'existe pas dans l'énumation => exception
            // Console.WriteLine(mr);

            // entier -> enum
            sbyte imr = 95;
            if (Enum.IsDefined(typeof(Motorisation), imr)) // Permet de tester si la valeur entière existe dans l'enumération
            {
                mr = (Motorisation)imr;
                Console.WriteLine(mr);
            }

            // Énumération comme indicateurs binaires
            JourSemaine js = JourSemaine.LUNDI | JourSemaine.MERCREDI;   //001 | 100 =101
            if ((js & JourSemaine.LUNDI) != 0)  // teste la présence de LUNDI
            {
                Console.WriteLine("Jour: Lundi");   // 101 & 001 =1
            }
            if ((js & JourSemaine.MARDI) != 0)  // teste la présence de MARDI
            {
                Console.WriteLine("Jour: Mardi");   // 101 & 010 =0
            }
            js |= JourSemaine.SAMEDI;
            if ((js & JourSemaine.WEEKEND) != 0)    // 0100101 & 1100000 = 0100000
            {
                Console.WriteLine("Jour: WeekEnd");
            }
            string strJs = js.ToString();
            Console.WriteLine(strJs);

            switch (m) // On peut utiliser une enumération avec un switch 
            {
                case Motorisation.DIESEL:
                    Console.WriteLine("Motorisation Diesel");
                    break;
                case Motorisation.ESSENCE:
                    Console.WriteLine("Motorisation Essence");
                    break;
                default:
                    Console.WriteLine("Autre motorisation");
                    break;
            }

            #endregion
            #region Structure
            Point p1;
            p1.x = 3;   // accès au champs X de la structure
            p1.y = 2;
            Console.WriteLine($"P1 x={p1.x} y={p1.y}");

            // copie
            Point p2 = p1; // copie de tous les champs

            Console.WriteLine($"P2 x={p2.x} y={p2.y}");
            p1.x = 5;

            if (p1.x == p2.x && p2.y == p1.y)
                Console.Write("Les point sont égaux");
            #endregion
            Console.ReadKey();
        }
    }
}
