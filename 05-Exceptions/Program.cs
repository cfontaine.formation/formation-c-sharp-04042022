﻿using System;

namespace _05_Exceptions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] t = new int[5];
                Console.WriteLine("Entrer un indice de l'élément du tableau");
                int index = Convert.ToInt32(Console.ReadLine());     // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier
                Console.WriteLine(t[index]);                         // Peut générer une Exception IndexOutOfRangeException , si index est > à Length
                Console.WriteLine("Entrer un age");
                int age = Convert.ToInt32(Console.ReadLine());       // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier
                TraitementEmploye(age);
                Console.WriteLine("Suite programme");
            }

            catch (IndexOutOfRangeException e)   // Attrape les exceptions IndexOutOfRangeException
            {
                Console.WriteLine($"Index en dehors des limites du tableau message={e.Message}");    // Message => permet de récupérer le messsage de l'exception
                Console.WriteLine(e.StackTrace);
            }
            catch (FormatException e)   // Attrape les exceptions FormatException
            {
                Console.WriteLine("Format incorrect");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            catch (Exception e)         // Attrape tous les autres exception
            {
                Console.WriteLine("Autre exception");
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                }
            }
            finally     // Le bloc finally est toujours éxécuté
            {
                // finally est utiliser pour libérer les ressources
                Console.WriteLine("finally -> toujours exécuté");
            }
            Console.WriteLine("Fin de programme");
            try
            {
                double v1 = Convert.ToDouble(Console.ReadLine());
                string op = Console.ReadLine();
                double v2 = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine(Calculatrice(v1, op, v2));
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            Console.ReadKey();
        }

        static void TraitementEmploye(int age)
        {
            Console.WriteLine("Début traitement Employe");
            try
            {
                TraitementAge(age);
            }
            catch (AgeNegatifException e) when (e.Age < -10)      // when : le catch est éxécuté si la condition dans when est vrai
            {

                Console.WriteLine($"Traitement local age <-10 {e.Message}");
                throw new ArgumentException("Erreur Traitement employé", e);    // On relance une exception différente  e => référence à l'exception qui a provoquer l'exception
            }
            catch (AgeNegatifException e) when (e.Age > -10)
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"Traitement local age >-10{e.Message}");
                throw;  // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau
            }
            Console.WriteLine("Fin traitement Employe");
        }

        static void TraitementAge(int age)
        {
            Console.WriteLine("Début traitement age");
            if (age < 0)
            {
                throw new AgeNegatifException(age, $"Age négatif: {age}");   //  throw => Lancer un exception
            }                                                                //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
            Console.WriteLine("Fin traitement age");                         //  si elle n'est pas traiter, aprés la méthode main => arrét du programme
        }

        static double Calculatrice(double v1, string op, double v2)
        {
            switch (op)
            {
                case "+":
                    return v1 + v2;
                case "-":
                    return v1 - v2;
                case "*":
                    return v1 * v2;
                case "/":
                    if (v2 == 0.0)
                    {
                        throw new DivideByZeroException();
                    }
                    else
                    {
                        return v1 / v2;
                    }
                default:
                    throw new Exception($"{op} n'est pas valide");
            }
        }
    }
}
