﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _04_Methodes
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            int res = Multiplier(10, 3);

            // Appel de methode (sans type retour)
            Afficher(res);

            // Exercice Maximum
            Console.WriteLine("Saisir 2 nombres entiers");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Maximum={Maximum(a, b)}");

            // Exercice Paire
            Console.WriteLine(Even(3));
            Console.WriteLine(Even(4));

            // Passage par valeur(par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int v1 = 23;
            TestParamValeur(v1);
            Console.WriteLine(v1);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamReference(ref v1);
            Console.WriteLine(v1);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée

            // int v2;

            // On peut déclarer la variable de retour dans les arguments pendant l'appel de la méthode
            TestParamOut(out int v2);
            Console.WriteLine(v2);

            // On peut ignorer un paramètre out en le nommant _
            TestParamOut(out _);

            // Passage de paramètre référence en entrée => in
            double v3 = 15;
            TestParamIn(v3);

            // Paramètre optionnel
            TestParamDefault(67, "Hello", 'r');
            TestParamDefault(6, "World");
            TestParamDefault(7);

            // Paramètres nommés
            TestParamDefault(8, c: 'T');
            TestParamDefault(c: 'Y', i: 34, str: "yuiop");

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne(2, 3, 4, 6, 6));
            Console.WriteLine(Moyenne(2, 3));
            Console.WriteLine(Moyenne(2));

            // Passage d'un tableau en paramètre
            int[] ta = { 2, 6, 8, 3, 7 };
            Console.WriteLine(Moyenne(2, ta));

            StringBuilder sb = new StringBuilder("Hello");
            TestParamValeurObj(sb);
            Console.WriteLine(sb);

            TestParamValeurObjModifEtat(sb);
            Console.WriteLine(sb);

            // Exercice Echange
            int c = Convert.ToInt32(Console.ReadLine());
            int d = Convert.ToInt32(Console.ReadLine());
            Swap(ref a, ref b);
            Console.WriteLine($"c={c} d={d}");

            // Exercice Tableau
            Menu();

            // Surcharge de méthode

            // Correspondance exacte des type des paramètres
            Console.WriteLine(Somme(1, 2));
            Console.WriteLine(Somme(1.5, 2.7));
            Console.WriteLine(Somme(1, 2.7));
            Console.WriteLine(Somme("Hello", " World"));
            Console.WriteLine(Somme(1, 2, 3));

            // Pas de correspondance exacte => convertion automatique
            Console.WriteLine(Somme(1, 2L));     // => appel de la méthode avec un entier et un double en paramètres
            Console.WriteLine(Somme(1L, 2L));    // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme('a', 'b'));  // => appel de la méthode avec 2 entiers en paramètres

            // Pas de conversion possible => Erreur
            // Console.WriteLine(Somme(2.3M, 5.6));

            // Méthode récursive
            int fact = Factorial(3);
            Console.WriteLine(fact);

            // Corps d'expression
            Console.WriteLine(Somme(2.4, 6));

            // Test méthode locale
            AffSquare(2.0);
            // Square(3.5); // La méthode locale n'est visible uniquement que dans la méthode AffSquare

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande
            foreach (var arg in args)
            {
                Console.WriteLine(arg);
            }

            // Exercice Rendeur de monnaie
            Console.Write("Entrer une somme (stocke inifinie) :");
            double somme = Convert.ToDouble(Console.ReadLine());
            Dictionary<double, int> pieces = RendeurMonnaieStockInfinie(somme);
            foreach (var kv in pieces)
            {
                Console.WriteLine($"{kv.Key} euro x {kv.Value}");
            }
            try
            {
                Console.Write("Entrer une somme :");
                somme = Convert.ToDouble(Console.ReadLine());
                int[] stockPiece = { 10, 10, 10, 10, 10, 10, 10, 10 };
                pieces = RendeurMonnaie(somme, stockPiece);
                foreach (var kv in pieces)
                {
                    Console.WriteLine($"{kv.Key} euro x {kv.Value}");
                }
                AfficherTab(stockPiece);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }

        // Déclaration
        static int Multiplier(int a, int b)
        {
            return a * b;   // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur de l'expression à droite
        }

        static void Afficher(int val)   // void => pas de valeur retourné
        {
            Console.WriteLine(val);
            // avec void => return; ou return peut être omis ;
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static int Maximum(int v1, int v2)
        {
            //if (v1 > v2) {
            //    return v1;
            //}
            //else
            //{
            //    return v2;
            //          
            return v1 > v2 ? v1 : v2;
        }
        #endregion

        #region Exercice Paire
        // Écrire une méthode Paire qui prend un entier en paramètre un entier
        // Elle retourne vrai, si il est paire
        static bool Even(int v)
        {
            //if (v % 2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return (v & 0b1) == 0; // ou v % 2 == 0;
        }
        #endregion

        #region Passage de paramètre

        // Passage par valeur
        static void TestParamValeur(int a)
        {
            Console.WriteLine(a);
            a = 12;     // La modification de la valeur du paramètre a n'a pas d'influence en dehors de la méthode
            Console.WriteLine(a);
        }

        // Passage par référence => ref
        static void TestParamReference(ref int a)
        {
            Console.WriteLine(a);
            a = 12;
            Console.WriteLine(a);
        }

        // Passage de paramètre en sortie => out
        static void TestParamOut(out int a)
        {
            // Console.WriteLine(a);    // erreur, on ne peutpas accéder en entrée à un paramètre out 
            a = 12;                     // La méthode doit obligatoirement affecter une valeur aux paramètres out
            Console.WriteLine(a);
        }

        // in => Passage par référence, mais uniquement en entrée
        // se comporte comme un passage par valeur, mais il n'y a pas de copie => gain en performance pour des structures (type valeur) contenant beaucoup de données 
        static void TestParamIn(in double d)
        {
            //d = 23.5; 
            double r = d * 2.5;
            Console.WriteLine(r);
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamDefault(int i, string str = "azerty", char c = 'a')
        {
            Console.WriteLine($"i={i} str={str} c={c}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(int a, params int[] varAgs)
        {
            double somme = a;
            foreach (int e in varAgs)
            {
                somme += e;
            }
            return somme / (varAgs.Length + 1);
        }

        static void TestParamValeurObj(StringBuilder sb)
        {
            Console.WriteLine(sb);
            sb = new StringBuilder("azerty");
            Console.WriteLine(sb);
        }

        static void TestParamValeurObjModifEtat(StringBuilder sb)
        {
            Console.WriteLine(sb);
            sb.Append(" World");
            Console.WriteLine(sb);
        }
        #endregion

        #region Exercice Echange
        // Écrire une méthode Swap qui prend en paramètre 2 entiers et qui permet d'inverser le contenu des 2 variables passées en paramètre
        static void Swap(ref int v1, ref int v2)
        {
            int tmp = v1;
            v1 = v2;
            v2 = tmp;
        }
        #endregion

        #region Exercice Tableau
        // - Écrire une méthode qui affiche un tableau d’entier

        // - Écrire une méthode qui permet de saisir :
        //   - La taille du tableau
        //   - Les éléments du tableau

        // - Écrire une méthode qui calcule :
        //   - le minimum d’un tableau d’entier
        //   - le maximum
        //   - la moyenne

        // - Faire un menu qui permet de lancer ces méthodes
        //      1 - Saisir le tableau
        //      2 - Afficher le tableau
        //      3 - Afficher le minimum, le maximum et la moyenne
        //      0 - Quitter
        //      Choix =
        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (int v in tab)
            {
                Console.Write($"{v} ");
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTab()
        {
            Console.Write("Entrer la taille du tableau :");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] t = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = Convert.ToInt32(Console.ReadLine());
            }
            return t;
        }

        static void CalculTab(int[] t, out int maximum, out double moyenne)
        {
            maximum = int.MinValue;
            double somme = 0.0;
            foreach (var val in t)
            {
                if (val > maximum)
                {
                    maximum = val;
                }
                somme += val;
            }
            moyenne = somme / t.Length;
        }

        static void AfficherMenu()
        {
            Console.WriteLine("\n1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
        }

        static void Menu()
        {
            int[] tab = null;
            int choix;
            AfficherMenu();
            do
            {
                Console.Write("Choix =");
                choix = Convert.ToInt32(Console.ReadLine());
                switch (choix)
                {
                    case 1:
                        tab = SaisirTab();
                        break;
                    case 2:
                        if (tab != null)
                        {
                            AfficherTab(tab);
                        }
                        break;
                    case 3:
                        if (tab != null)
                        {
                            CalculTab(tab, out int max, out double moyenne);
                            Console.WriteLine($"Maximum={max} Moyenne={moyenne}");
                        }
                        break;
                    case 0:
                        Console.WriteLine("Au revoir!");
                        break;
                    default:
                        Console.WriteLine($"Le choix {choix} n'existe pas ");
                        AfficherMenu();
                        break;
                }
            }
            while (choix != 0);
        }
        #endregion

        #region Surcharge de méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Somme(int a, int b)
        {
            Console.WriteLine("Somme 2 entiers");
            return a + b;
        }

        static double Somme(double a, double b)
        {
            Console.WriteLine("Somme 2 doubles");
            return a + b;
        }

        static double Somme(int a, double b)
        {
            Console.WriteLine("Somme un entier un double");
            return a + b;
        }

        static string Somme(string s1, string s2)
        {
            Console.WriteLine("Somme de deux chaine");
            return s1 + s2;
        }

        static int Somme(int v1, int v2, int v3)
        {
            Console.WriteLine("Somme 3 entiers");
            return v1 + v2 + v3;
        }
        #endregion

        #region recursivité
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1) // condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion

        // Corps de méthode
        // une méthode qui comprend une seule expression, peut s'écrire avec l'opérateur =>
        static double Somme(double b, int a) => a + b;

        // Méthode locale
        static void AffSquare(double v)
        {
            // Console.WriteLine(Square(v));

            // Méthode locale
            // Une méthode locale n'est visible que par la méthode englobante
            // double Square(double a) => a * a;

            // Capture
            // Une méthode locale peut accéder aux variables locales et aux paramètres de la méthode englobante
            double w = v;
            Console.WriteLine(Square());
            double Square() => v * w;
        }

        #region Exercice rendeur de monnaie
        // Rendeur de monnaie
        // On dispose de pièce de 2€ 1€ 0,5€ 0,2€ 0,1€ 0,05€ 0,02€ 0,01€
        // - L'utilisateur saisi une somme en euro à rendre 
        // - Calculer et afficher le nombre minimum de chaque type pièce nécessaire pour rendre cette somme  
        // 1. Avec un nombre de pièce illimitée
        // 2. Avec un stock initial de 10 pour chaque type de pièce.Dans ce cas, on indique aussi, si l'on ne peut pas rendre la monnaie 
        static Dictionary<double, int> RendeurMonnaieStockInfinie(double sommeEuro)
        {
            int[] valeurPieces = { 200, 100, 50, 20, 10, 5, 2, 1 };
            Dictionary<double, int> pieces = new Dictionary<double, int>();
            int val = (int)(sommeEuro * 100.0);
            int i = 0;
            while (val > 0.0 && i < valeurPieces.Length)
            {
                int tmp = val / valeurPieces[i];
                if (tmp != 0)
                {
                    pieces.Add(valeurPieces[i] / 100.0, tmp);
                    val -= tmp * valeurPieces[i];
                }
                i++;
            }
            return pieces;
        }

        static Dictionary<double, int> RendeurMonnaie(double sommeEuro, int[] stockPiece)
        {
            int[] valeurPieces = { 200, 100, 50, 20, 10, 5, 2, 1 };
            if (valeurPieces.Length != stockPiece.Length)
            {
                throw new Exception("Le tableau de stocke n'est pas bien dimmensionner");
            }
            Dictionary<double, int> pieces = new Dictionary<double, int>();
            int val = (int)(sommeEuro * 100.0);
            int i = 0;
            while (val > 0.0 && i < valeurPieces.Length)
            {
                int nbPiece = val / valeurPieces[i];
                if (nbPiece != 0)
                {
                    if (nbPiece > stockPiece[i])
                    {
                        nbPiece = stockPiece[i];
                    }
                    pieces.Add(valeurPieces[i] / 100.0, nbPiece);
                    val -= nbPiece * valeurPieces[i];
                    stockPiece[i] -= nbPiece;
                }
                i++;
            }
            if (val > 0.0)
            {
                throw new Exception($"Il n'y a pas assez de pièce dans le stock  reste:{val / 100.0} euros");
            }
            return pieces;
        }
        #endregion
    }
}
